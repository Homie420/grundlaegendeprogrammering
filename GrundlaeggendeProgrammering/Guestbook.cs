﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class FileHandler
    {
        const string rootPath = @".\";
        string filePath;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="filepath"></param>
        public FileHandler(string filepath)
        {
            this.filePath = rootPath + filepath;

            if (!this.FileExists())
                this.CreateFile();
 
        }

        /// <summary>
        /// Returns whether the file exists or not
        /// </summary>
        /// <returns></returns>
        bool FileExists()
        {
            return File.Exists(this.filePath);
        }

        /// <summary>
        /// Create the file
        /// </summary>
        void CreateFile()
        {
            FileStream fs = new FileStream(this.filePath, FileMode.Create);
            fs.Close();
        }

        /// <summary>
        /// Returns the content as lines
        /// </summary>
        /// <returns></returns>
        public string[] GetContentAsLines()
        {
            return File.ReadAllLines(this.filePath);
        }

        /// <summary>
        /// Get the content as a 2d array
        /// </summary>
        /// <returns></returns>
        public string[,] GetContentAs2dArray()
        {
            string[] _content = this.GetContentAsLines();
            int _nmbsOfArr = _content[0].Split('|').Length;

            string[,] _nContent = new string[_content.Length, _nmbsOfArr];

            for (int i = 0; i < _content.Length; i++)
            {
                string[] _lineArr = _content[i].Split('|');

                for (int j = 0; j < _lineArr.Length; j++)
                {
                    _nContent[i, j] = _lineArr[j];
                }
            }

            return _nContent;
        }

        /// <summary>
        /// appends a line of content to the file
        /// </summary>
        /// <param name="Content"></param>
        public void AppendToFile(string Content)
        {
            string[] _curData = this.GetContentAsLines();
            string[] _newData = new string[_curData.Length + 1];

            for (int i = 0; i < _curData.Length; i++)
            {
                _newData[i] = _curData[i];
            }

            _newData[_curData.Length] = Content;

            this.WriteToFile(_newData);
        }

        /// <summary>
        /// Write string content to file
        /// </summary>
        /// <param name="content"></param>
        public void WriteToFile(string content)
        {
            File.WriteAllText(this.filePath, content);
        }

        /// <summary>
        /// Write String array to file
        /// </summary>
        /// <param name="content"></param>
        public void WriteToFile(string[] content)
        {
            File.WriteAllLines(this.filePath, content);
        }

        /// <summary>
        /// Write 2d array to file [?, 3]
        /// </summary>
        /// <param name="content"></param>
        public void WriteToFile(string[,] content)
        {
            string[] _content = new string[content.GetLength(0)];

            for (int i = 0; i < content.GetLength(0); i++)
            {
                for (int j = 0; j < content.GetLength(1); j++)
                {
                    _content[i] += content[i, j];

                    if (j + 1 != content.GetLength(1))
                        _content[i] += "|";
                }
            }

            File.WriteAllLines(this.filePath, _content);
        }

        /// <summary>
        /// Prints the content out
        /// </summary>
        public string GetPrintContent()
        {
            string[,] _tmp = this.GetContentAs2dArray();
            string _output = "";

            for (int i = 0; i < _tmp.GetLength(0); i++)
            {
                for (int j = 0; j < _tmp.GetLength(1); j++)
                {
                    _output += _tmp[i, j] + "\t";
                }
                _output += "\n";
            }

            return _output;
        }

        /// <summary>
        /// Deletes a line in the file from the index given
        /// </summary>
        /// <param name="index"></param>
        public void DeleteLine(int index)
        {
            string[] _content = this.GetContentAsLines();
            List<string> _newContent = new List<string>();

            for (int i = 0; i < _content.Length; i++)
            {
                if (i != index)
                    _newContent.Add(_content[i]);
            }

            this.WriteToFile(_newContent.ToArray());
        }
    }

    class User
    {

        public string Username { get { return this.username; } }
        public int Power { get { return this.power; } }

        FileHandler accFile;
        string username;
        string password;
        int power = 0;

        public User(FileHandler accFile)
        {
            this.accFile = accFile;
        }

        /// <summary>
        /// Checks if an username exists
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public bool CheckUsername(string username)
        {
            string[,] _userData = this.accFile.GetContentAs2dArray();

            for (int i = 0; i < _userData.GetLength(0); i++)
            {
                if (_userData[i, 0] == username)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if a password exists
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool CheckPassword(string password)
        {
            string[,] _userData = this.accFile.GetContentAs2dArray();

            for (int i = 0; i < _userData.GetLength(0); i++)
            {
                if (_userData[i, 1] == password)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Gets the power from the user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public int GetPower(string username, string password)
        {
            string[,] _userData = this.accFile.GetContentAs2dArray();

            for (int i = 0; i < _userData.GetLength(0); i++)
            {
                if (_userData[i, 0] == username && _userData[i, 1] == password)
                    return Int32.Parse(_userData[i, 2]);
            }

            return 0;
        }

        /// <summary>
        /// Set the current user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="power"></param>
        public void SetCurrentUser(string username, string password, int power)
        {
            this.username = username;
            this.password = password;
            this.power = power;
        }

        /// <summary>
        /// Check if someone is logined
        /// </summary>
        /// <returns></returns>
        public bool IsUserLogined()
        {
            if (this.username != null && this.password != null)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Logouts an user
        /// </summary>
        public void Logout()
        {
            this.username = null;
            this.password = null;
            this.power = 0;
        }

        /// <summary>
        /// Creates an user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="power"></param>
        public void CreateUser(string username, string password, int power)
        {
            string _user = username + "|" + password + "|" + power;
            this.accFile.AppendToFile(_user);
        }
    }

    class Guestbook : IProg
    {
        public string ProgName { get { return "GuestBook"; } }

        const string guestFileName = "Guests.txt";
        const string userFileName = "Users.txt";

        FileHandler guestFile;
        FileHandler userFile;

        User currentUser;

        /// <summary>
        /// The Login GUI
        /// </summary>
        void LoginMenuGUI()
        {
            // Get User Data

            Console.Clear();
            Console.WriteLine("==========================");
            Console.WriteLine("=====Guest Book Login=====");
            Console.WriteLine("==========================");

            bool _tryUsername = true;
            bool _tryPassword = false;
            string _userInputUsername = null;
            string _userInputPassword = null;

            while(_tryUsername)
            {
                Console.Write("Enter Username: ");
                _userInputUsername = Console.ReadLine();

                if (!currentUser.CheckUsername(_userInputUsername))
                    Console.WriteLine("Username Do not match!");
                else
                    _tryUsername = false;
            }

            for (int i = 0; i < 3; i++)
            {
                Console.Write("Enter Password: ");
                _userInputPassword = Console.ReadLine();

                if (!currentUser.CheckPassword(_userInputPassword))
                    Console.WriteLine("Password do not match");
                else
                {
                    int _power = currentUser.GetPower(_userInputUsername, _userInputPassword);
                    this.currentUser.SetCurrentUser(_userInputUsername, _userInputPassword, _power);
                    _tryPassword = true;
                    i = 4;
                }
            }

            if (_tryPassword)
            {
                Console.WriteLine("Welcome in {0}", _userInputUsername);
                this.StartMenuGUI();
            }
            else
            {
                Console.WriteLine("You have exceeded the limit of tries!");
                this.StartMenuGUI();
            }
        }

        void CreateAccountMenuGUI()
        {
            Console.Clear();
            Console.WriteLine("==========================");
            Console.WriteLine("====Guest Book Account====");
            Console.WriteLine("==========================");
            Console.WriteLine("Create New Account");

            bool _tryUsername = true;
            string _userNameInput = null;

            while (_tryUsername)
            {
                Console.Write("Enter Username: ");
                _userNameInput = Console.ReadLine();

                _tryUsername = this.currentUser.CheckUsername(_userNameInput);
            }

            Console.WriteLine(_userNameInput + ":");
            Console.WriteLine("Enter password");

            string _userPassInput = Console.ReadLine();

            this.currentUser.CreateUser(_userNameInput, _userPassInput, 0);

            Console.WriteLine("Congratulation your account has been created!");
            Console.WriteLine("Press key to go to start menu!");

            Console.ReadKey();

            this.StartMenuGUI();

        }

        /// <summary>
        /// The Anon GUI Menu
        /// </summary>
        void AnonGuestGUI()
        {
            Console.Clear();
            Console.WriteLine("==========================");
            Console.WriteLine("=====Anon Guest Book======");
            Console.WriteLine("==========================");
            Console.WriteLine("1. Write Message");
            Console.WriteLine("?. Go Back");
            Console.Write("Enter Choice: ");

            string _userInput = Console.ReadLine();

            switch(_userInput)
            {
                case "1":
                    Console.WriteLine("Enter Message");
                    string _messageInput = Console.ReadLine();
                    string _line = "Anon|Anon@anon.dk|" + _messageInput;
                    this.guestFile.AppendToFile(_line);
                    this.StartMenuGUI();
                    break;
                default:
                    this.StartMenuGUI();
                    break;
            }
        }

        void DisplayGuestBookGUI()
        {
            Console.Clear();
            Console.WriteLine("==========================");
            Console.WriteLine("=====Display Guest Book===");
            Console.WriteLine("==========================");
            Console.WriteLine(guestFile.GetPrintContent());

            Console.ReadKey();

            this.StartMenuGUI();
        }

        void LoginedMenuGUI()
        {
            Console.Clear();
            Console.WriteLine("==========================");
            Console.WriteLine("=======Guest Book=========");
            Console.WriteLine("==========================");
            Console.WriteLine("Welcome {0}", this.currentUser.Username);

            Console.WriteLine("1. Write Message");
            Console.WriteLine("2. Display GuestBook");
            Console.WriteLine("3. Delete Message");
            Console.WriteLine("?. Logout");
            Console.Write("Enter Choice: ");

            string _userInput = Console.ReadLine();

            switch(_userInput)
            {
                case "1":
                    Console.Write("Write Message: ");
                    string _userMessage = Console.ReadLine();
                    string _line = this.currentUser.Username + "|" + this.currentUser.Username + "@hotmail.com" + "|" + _userMessage;
                    this.guestFile.AppendToFile(_line);
                    this.StartMenuGUI();
                    break;
                case "2":
                    this.DisplayGuestBookGUI();
                    break;
                case "3":
                    if (this.currentUser.Power > 0)
                        this.DeleteMessageMenuGUI();
                    else
                        this.StartMenuGUI();
                    break;
                default:
                    this.currentUser.Logout();
                    this.StartMenuGUI();
                    break;
            }  
        }

        /// <summary>
        /// Delete Message GUI
        /// </summary>
        void DeleteMessageMenuGUI()
        {
            Console.Clear();
            Console.WriteLine("==========================");
            Console.WriteLine("Guest Book Delete Message");
            Console.WriteLine("==========================");
            string[] _guestbookData = this.guestFile.GetContentAsLines();

            for (int i = 0; i < _guestbookData.Length; i++)
            {
                Console.WriteLine("{0}. {1}", i, _guestbookData[i]);
            }
            Console.Write("Enter number to delete: ");
            int _userInput = Int32.Parse(Console.ReadLine());

            if (_userInput >= 0 && _userInput <= _guestbookData.Length - 1)
                this.guestFile.DeleteLine(_userInput);

            Console.WriteLine(_guestbookData[_userInput] + " Has been deleted");

            Console.ReadKey();

            this.StartMenuGUI();
        }



        /// <summary>
        /// The Start Menu GUI
        /// </summary>
        void StartMenuGUI()
        {
            // Check if an user is already logined
            if (currentUser.IsUserLogined())
                this.LoginedMenuGUI();

            Console.Clear();
            Console.WriteLine("==========================");
            Console.WriteLine("=======Guest Book=========");
            Console.WriteLine("==========================");
            Console.WriteLine("1. Log in");
            Console.WriteLine("2. Anonymous");
            Console.WriteLine("3. Create Account");
            Console.WriteLine("4. Display Guestbook");
            Console.WriteLine("?. Exit");
            Console.Write("Enter Choice: ");
            string _userInput = Console.ReadLine();


            switch (_userInput)
            {
                case "1":
                    this.LoginMenuGUI();
                    break;
                case "2":
                    this.AnonGuestGUI();
                    break;
                case "3":
                    this.CreateAccountMenuGUI();
                    break;
                case "4":
                    this.DisplayGuestBookGUI();
                    break;
                default:
                    break;
            }
        }

        void Start()
        {
            // Assign values and create objects
            guestFile = new FileHandler(guestFileName);
            userFile = new FileHandler(userFileName);

            currentUser = new User(userFile);

            this.StartMenuGUI();
        }

        public void Run()
        {
            this.Start();
        }
    }
}
