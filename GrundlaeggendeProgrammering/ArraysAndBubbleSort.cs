﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class ArraysAndBubbleSortA : IProg
    {
        int[] nArr = new int[100];

        Random rnd = new Random();

        public string ProgName { get { return "Arrays And Bubble sort Opgave A"; } }

        /// <summary>
        /// The start method to assign values to the array
        /// </summary>
        void Start()
        {
            for (int i = 0; i < nArr.Length; i++)
            {
                nArr[i] = rnd.Next(1, 1000);
            }
        }

        /// <summary>
        /// Prints the array in a nice way
        /// </summary>
        void PrintArray()
        {
            for (int i = 0; i < nArr.Length; i++)
            {
                if (i != 0 && (i+1)%5 == 0)
                    Console.WriteLine("\n");
                else
                    Console.Write(nArr[i] + "\t");
            }
        }

        /// <summary>
        /// The run function.
        /// </summary>
        public void Run()
        {
            this.Start();

            this.PrintArray();
        }
    }

    class ArrayAndBubbleSortB : IProg
    {
        public string ProgName { get { return "Bubble Sort Opgave B"; } } // The name of the mini program

        Random rnd = new Random();

        int[] nArr = new int[10];

        /// <summary>
        /// The start method
        /// </summary>
        void Start()
        {
            for (int i = 0; i < nArr.Length; i++)
            {
                nArr[i] = rnd.Next(1, 100);
            }

            this.PrintArray();
        }

        /// <summary>
        /// Prints the array
        /// </summary>
        void PrintArray()
        {
            for (int i = 0; i < nArr.Length; i++)
            {
                Console.Write(nArr[i] + "\t");
            }
            Console.WriteLine("");
        }

        /// <summary>
        ///  Sort the array
        /// </summary>
        void BubbleSort()
        {
            for (int i = 0; i < nArr.Length; i++) // iterations left
            {
                int _iterations = nArr.Length - i;

                for (int j = 0; j < _iterations; j++)
                {
                    int _nextIndex = j;

                    if (j != _iterations - 1)
                        _nextIndex++;

                    if (nArr[j] > nArr[_nextIndex]) // Swap values
                    {
                        int tmpVal = nArr[j];

                        nArr[j] = nArr[_nextIndex];
                        nArr[_nextIndex] = tmpVal;
                    }
                }

                this.PrintArray();
            }
        }




        /// <summary>
        /// The run method
        /// </summary>
        public void Run()
        {
            this.Start();

            this.BubbleSort();
        }
    }

    class ArrayAndBubbleSortC : IProg
    {
        public string ProgName { get { return "Array And Sort Bubble Opgave C"; } }

        Random rnd = new Random();

        int[] nArr = new int[10];

        /// <summary>
        /// The start method
        /// </summary>
        void Start()
        {
            for (int i = 0; i < nArr.Length; i++)
            {
                nArr[i] = rnd.Next(1, 100);
            }

            this.PrintArray(nArr);
        }

        /// <summary>
        /// Prints the array
        /// </summary>
        void PrintArray(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i] + "\t");
            }
            Console.WriteLine("");
        }

        /// <summary>
        ///  Sort the array
        /// </summary>
        void BubbleSort()
        {
            for (int i = 0; i < nArr.Length; i++) // iterations left
            {
                int _iterations = nArr.Length - i;

                for (int j = 0; j < _iterations; j++)
                {
                    int _nextIndex = j;

                    if (j != _iterations - 1)
                        _nextIndex++;

                    if (nArr[j] > nArr[_nextIndex]) // Swap values
                    {
                        int tmpVal = nArr[j];

                        nArr[j] = nArr[_nextIndex];
                        nArr[_nextIndex] = tmpVal;
                    }
                }

                this.PrintArray(nArr);
            }
        }

        /// <summary>
        /// Reverses the nArray
        /// </summary>
        /// <returns></returns>
        int[] ReverseTable()
        {
            int[] tmpArr = nArr;

            Array.Reverse(tmpArr, 0, tmpArr.Length);

            return tmpArr;
        }


        /// <summary>
        /// The run method
        /// </summary>
        public void Run()
        {
            this.Start();

            this.BubbleSort();

            int[] revArr = this.ReverseTable();

            Console.WriteLine("Reversed");
            this.PrintArray(revArr);
        }
    }
}
