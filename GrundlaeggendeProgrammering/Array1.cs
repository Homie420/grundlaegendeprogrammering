﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class Array1 : IProg
    {
        int[] numberArr = new int[9];

        public string ProgName { get { return "Array1"; } }

        /// <summary>
        /// Start putting numbers into the array
        /// </summary>
        void Start()
        {
            for (int i = 0; i < numberArr.Length; i++)
            {
                numberArr[i] = i + 1;
            }
        }

        /// <summary>
        /// Run the program and change the number at index 4 (5th number) in the array
        /// </summary>
        public void Run()
        {
            this.Start();

            for (int i = 0; i < numberArr.Length; i++)
            {
                if (i == 4)
                    numberArr[i] = numberArr[i - 1] * 2;

                Console.WriteLine(numberArr[i]);
            }
        }
    }
}
