﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class Morsekode : IProg
    {
        // The lookup table
        Dictionary<char, string> morseTable = new Dictionary<char, string>()
        {
            ['a'] = ".-",
            ['b'] = "-...",
            ['c'] = "-.-.",
            ['d'] = "-..",
            ['e'] = ".",
            ['f'] = "..-",
            ['g'] = "--.",
            ['h'] = "....",
            ['i'] = "..",
            ['j'] = ".---",
            ['k'] = "-.-",
            ['l'] = ".-..",
            ['m'] = "--",
            ['n'] = "-.",
            ['o'] = "---",
            ['p'] = ".--.",
            ['q'] = "--.-",
            ['r'] = ".-.",
            ['s'] = "...",
            ['t'] = "-",
            ['u'] = "..-",
            ['v'] = "...-",
            ['w'] = ".--",
            ['x'] = "-..-",
            ['y'] = "-.--",
            ['z'] = "--..",
            ['æ'] = ".-.-",
            ['ø'] = "---.",
            ['å'] = ".--.-"
        };

        public string ProgName { get { return "Morsekode"; } }

        /// <summary>
        /// Convert the word to lower and print out the corresponding morse code to the chars in the string
        /// </summary>
        /// <param name="word"></param>
        void ConvertToMorseAndPrint(string word)
        {
            string wLower = word.ToLower();

            Console.WriteLine("Table Lookup:");

            for (int i = 0; i < wLower.Length; i++)
                Console.Write(morseTable[wLower[i]]);
        }

        /// <summary>
        /// Converts to morse code by using a switch statement
        /// </summary>
        /// <param name="word"></param>
        void ConvertToMorseAndPrintSwitch(string word)
        {
            string mLower = word.ToLower();

            Console.WriteLine("\n\nSwitch Lookup: ");

            for (int i = 0; i < mLower.Length; i++)
            {
                string outMorse;

                switch(mLower[i])
                {
                    case 'a':
                        outMorse = ".-";
                        break;
                    case 'b':
                        outMorse = "-...";
                        break;
                    case 'c':
                        outMorse = "-.-.";
                        break;
                    case 'd':
                        outMorse = "-..";
                        break;
                    case 'e':
                        outMorse = ".";
                        break;
                    case 'f':
                        outMorse = "..-";
                        break;
                    case 'g':
                        outMorse = "--.";
                        break;
                    case 'h':
                        outMorse = "....";
                        break;
                    case 'i':
                        outMorse = "..";
                        break;
                    case 'j':
                        outMorse = ".---";
                        break;
                    case 'k':
                        outMorse = "-.-";
                        break;
                    case 'l':
                        outMorse = ".-..";
                        break;
                    case 'm':
                        outMorse = "--";
                        break;
                    case 'n':
                        outMorse = "-.";
                        break;
                    case 'o':
                        outMorse = "---";
                        break;
                    case 'p':
                        outMorse = ".--.";
                        break;
                    case 'q':
                        outMorse = "--.-";
                        break;
                    case 'r':
                        outMorse = ".-.";
                        break;
                    case 's':
                        outMorse = "...";
                        break;
                    case 't':
                        outMorse = "-";
                        break;
                    case 'u':
                        outMorse = "..-";
                        break;
                    case 'v':
                        outMorse = "...-";
                        break;
                    case 'w':
                        outMorse = ".--";
                        break;
                    case 'x':
                        outMorse = "-..-";
                        break;
                    case 'y':
                        outMorse = "-.--";
                        break;
                    case 'z':
                        outMorse = "--..";
                        break;
                    case 'æ':
                        outMorse = ".-.-";
                        break;
                    case 'ø':
                        outMorse = "---.";
                        break;
                    case 'å':
                        outMorse = ".--.-";
                        break;
                    default:
                        outMorse = "?";
                        break;
                }

                Console.Write(outMorse);
            }
        }

        /// <summary>
        /// Run the program
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Write a word that you want translated to morse");
            string userWord = Console.ReadLine();

            Console.WriteLine("You have choosen " + userWord + "\n");

            this.ConvertToMorseAndPrint(userWord);

            this.ConvertToMorseAndPrintSwitch(userWord);
        }
    }
}
