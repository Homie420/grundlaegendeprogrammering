﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    interface IProg
    {
        string ProgName { get; }
        void Run();
    }
}
