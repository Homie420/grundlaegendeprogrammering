﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class Pythagoras : IProg
    {
        public string ProgName { get { return "Pythagoras"; } }

        /// <summary>
        /// Get the pythagoras value of 2 numbers in ^2
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        double GetPythagoras(double a, double b)
        {
            return (a * a) + (b * b);
        }

        /// <summary>
        /// Runs the program the program takes in 2 numbers from the user, and ouputs pythagoras and which number is bigger than the other, of the 2 numbers the user typed.
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Pythargoras \n");

            Console.WriteLine("First number: ");
            double a = Double.Parse(Console.ReadLine());

            Console.WriteLine("Second Number: ");
            double b = Double.Parse(Console.ReadLine());

            double c = this.GetPythagoras(a, b);
            double cSqrt = Math.Sqrt(c);

            Console.WriteLine("The C^2 value is: " + c);
            Console.WriteLine("The Value of c is: " + cSqrt);

            // Check which number is bigger than the other.
            if (a < b)
                Console.WriteLine("b is bigger than a");
            else if (a > b)
                Console.WriteLine("a is bigger than b");
            else
                Console.WriteLine("They are equal to each other");

        }
    }
}
