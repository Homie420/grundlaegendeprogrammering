﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class VariableOgDatatyper : IProg
    {


        readonly int a = 16;
        readonly double b = 5.1;
        int c;
        double d;

        public string ProgName { get { return "Variable Og Datatyper"; } }

        public void OpgaveA()
        {

            Console.WriteLine(a + b);
        }

        public void OpgaveB()
        {
            // The variable cannot be assigned because variable b is of type double, and c is an int.
            // Would have to cast b as an int.
            // WRONG: c = a + b;

            c = a + (int)b;
        }

        public void OpgaveC()
        {
            // The variable d can be assigned because it is of type double, and the datatype of a + b will be a double.
            d = a + b;
        }

        /// <summary>
        /// Runs the program
        /// </summary>
        public void Run()
        {
            this.OpgaveA();
            this.OpgaveB();
            this.OpgaveC();
        }
    }
}
