﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class ValutaOmregner : IProg
    {
        const double dkUs = 0.15;
        const double dkEuro = 0.13;
        const double dkSwedish = 1.37;
        const double dkPound = 0.12;

        public string ProgName { get { return "Valuta Omregner"; } }

        // US
        /// <summary>
        /// Returns the danish valuta in Us Dollars
        /// </summary>
        /// <param name="dkUser"></param>
        /// <returns></returns>
        double GetUs(double dkUser)
        {
            return dkUser * dkUs;
        }

        // Pound
        /// <summary>
        /// Get danish valuta in pounds
        /// </summary>
        /// <param name="dkUser"></param>
        /// <returns></returns>
        double GetPound(double dkUser)
        {
            return dkUser * dkPound;
        }

        // Euro
        
        /// <summary>
        /// Get the danish valuta in euro
        /// </summary>
        /// <param name="dkUser"></param>
        /// <returns></returns>
        double GetEuro(double dkUser)
        {
            return dkUser * dkEuro;
        }

        // Swedish

        /// <summary>
        /// Get the danish valuta in swedish valuta
        /// </summary>
        /// <param name="dkUser"></param>
        /// <returns></returns>
        double GetSwedish(double dkUser)
        {
            return dkUser * dkSwedish;
        }

        /// <summary>
        /// Runs the program and outputs the results to the console.
        /// </summary>
        /// <param name="dkUserInput"></param>
        public void Run()
        {
            Console.WriteLine("Type in the amount of Danish Valuta you want to convert: ");

            string userInput = Console.ReadLine();

            double dkUser = Double.Parse(userInput);

            Console.WriteLine("Danish: " + dkUser + "Kr. US: " + this.GetUs(dkUser) + "$. Pound: " + this.GetPound(dkUser) + "£. Euro: " + this.GetEuro(dkUser) + "EUR. Swedish: " + this.GetSwedish(dkUser) + "SEK.");
        }

    }
}
