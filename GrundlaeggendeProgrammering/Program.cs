using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class Program
    {
        static void Main(string[] args)
        {
            #region Objects
            IProg[] _programs =
            {
                new VariableOgDatatyper(),
                new Celciusomregner(),
                new ValutaOmregner(),
                new Rumfanget(),
                new Terningkast(),
                new Pythagoras(),
                new Alder(),
                new GaetEtTal(),
                new Porto(),
                new Morsekode(),
                new IgangMedLoekker(),
                new Array1(),
                new UserApplication(),
                new BoyNameSearcher(),
                new BoysAndGirlsName(),
                new Karakterer(),
                new StatistikProgram(),
                new ExcelTable(),
                new EditableExcelTable(),
                new ArraysAndBubbleSortA(),
                new ArrayAndBubbleSortB(),
                new ArrayAndBubbleSortC(),
                new Liste(),
                new Lotto(),
                new Mozart(),
                new InputMethods(),
                new LoopMethods(),
                new SelfDefinedMethods(),
                new FileMethods(),
                new Guestbook(),


            };

            #endregion

            #region Menu

            void StartMenu()
            {
                Console.ForegroundColor = ConsoleColor.Green;

                Console.WriteLine("Pick a program to run");

                for (int i = 0; i < _programs.Length; i++)
                {
                    Console.WriteLine(i + ". " + _programs[i].ProgName);
                }

                int answer = Int32.Parse(Console.ReadLine());

                Console.ForegroundColor = ConsoleColor.White;

                // Run Program
                if (answer >= 0 && answer < _programs.Length)
                {
                    Console.Clear();
                    _programs[answer].Run();
                }

                Console.WriteLine("\n\n\nPress Key To Go To Startmenu");
                Console.ReadKey();
                Console.Clear();

                StartMenu();
            }

            #endregion

            // Start The Program
            StartMenu();

            Console.ReadKey();
        }
    }
}
