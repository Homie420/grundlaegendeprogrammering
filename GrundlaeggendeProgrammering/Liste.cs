﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class Liste : IProg
    {
        public string ProgName { get { return "Liste"; } }

        List<int> nList = new List<int>();

        /// <summary>
        /// The start method
        /// </summary>
        void Start()
        {
            for (int i = 0; i <= 20; i++)
            {
                if (i != 0 && i % 2 == 0)
                    nList.Add(i);
            }
            this.PrintList(nList);
        }

        /// <summary>
        /// remove specific numbers
        /// </summary>
        void Remove3s()
        {
            Console.WriteLine("Removing the numbers which modulus with 3 is equal to zero");
            for (int i = 0; i < nList.Count(); i++)
            {
                if (nList[i] % 3 == 0)
                    nList.RemoveAt(i);
            }

            this.PrintList(nList);

            Console.WriteLine("Add 17 at the index 3");
            nList.Insert(2, 17);

            this.PrintList(nList);
        }

        /// <summary>
        /// Prints the list values to the console
        /// </summary>
        /// <param name="b"></param>
        void PrintList(IEnumerable<int> b)
        {
            for (int i = 0; i < b.Count(); i++)
            {
                Console.Write(nList[i] + "\t");
            }
            Console.WriteLine("");
        }
        
        /// <summary>
        /// Reverses the list and returns a new
        /// </summary>
        /// <returns></returns>
        List<int> ReverseList()
        {
            List<int> tmpList = nList;

            tmpList.Reverse();

            return tmpList;
        }

        /// <summary>
        /// the method which run the mini program
        /// </summary>
        public void Run()
        {
            this.Start();
            this.Remove3s();

            Console.WriteLine("Reversing the list");
            List<int> rev = this.ReverseList();
            this.PrintList(rev);
        }
    }
}
