﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{

    enum PackageType
    {
        Letter,
        Box
    }
    enum Country
    {
        Inside,
        Outside
    }

    class Porto : IProg
    {
        double length; // the length is in cm
        double weigth; // the weight is in kg
        PackageType type;
        Country country;

        public string ProgName { get { return "Porto"; } }

        void GetPackageDimensions()
        {
            // Get the length
            Console.WriteLine("Write the length of the package in cm");
            this.length = Double.Parse(Console.ReadLine());

            // get the weigth of the package
            Console.WriteLine("Write the weight of the package in kg");
            this.weigth = Double.Parse(Console.ReadLine());
        }
        
        /// <summary>
        /// Get the packagetype. It is decided by the weight and the length, or the users input
        /// </summary>
        /// <returns></returns>
        PackageType GetPackageType()
        {
            // Check if forced to ship as box
            if (this.weigth > 2.0 || this.length > 60)
            {
                return PackageType.Box;
            }
            else // Ask user how it should be shipped
            {
                Console.WriteLine("Choose the way you want to ship the package. Type 1. For letter, any other number for Box");
                int answer = Int32.Parse(Console.ReadLine());

                if (answer == 1)
                    return PackageType.Letter;
                else
                    return PackageType.Box;
            }
        }

        /// <summary>
        /// Get the package destination from the users input
        /// </summary>
        /// <returns></returns>
        Country GetPackageDestination()
        {
            // Get user input
            Console.WriteLine("Choose if you want to send it to Denmark or outside. 'DK' for Denmark and anything else for outside the country");
            string answer = Console.ReadLine();

            // Check answer and return the right value
            if (answer == "DK")
                return Country.Inside;
            else
                return Country.Outside;
        }

        /// <summary>
        /// returns the price for a letter sent in denmark
        /// </summary>
        /// <returns></returns>
        double GetPriceDenmarkLetter()
        {
            if (this.weigth <= 50)
                return 10;
            else if (this.weigth > 50 && this.weigth <= 100)
                return 20;
            else if (this.weigth > 100 && this.weigth <= 250)
                return 40;
            else
                return 60;
        }

        /// <summary>
        /// Get the price for a letter sent to other countries than denmark.
        /// </summary>
        /// <returns></returns>
        double GetPriceOutLetter()
        {
            if (this.weigth <= 50)
                return 30;
            else if (this.weigth > 50 && this.weigth <= 100)
                return 30;
            else if (this.weigth > 100 && this.weigth <= 250)
                return 60;
            else
                return 90;
        }

        /// <summary>
        /// Get the price for sending a box inside denmark
        /// </summary>
        /// <returns></returns>
        double GetPriceDenmarkBox()
        {
            if (this.weigth <= 1000)
                return 60;
            else if (this.weigth > 1000 && this.weigth <= 2000)
                return 65;
            else if (this.weigth > 2000 && this.weigth <= 5000)
                return 70;
            else if (this.weigth > 5000 && this.weigth <= 10000)
                return 90;
            else if (this.weigth > 10000 && this.weigth <= 20000)
                return 160;
            else if (this.weigth > 20000 && this.weigth <= 25000)
                return 200;
            else if (this.weigth > 25000 && this.weigth <= 30000)
                return 225;
            else
                return 250;
        }

        /// <summary>
        /// Get the price for sending a box outside denmark.
        /// </summary>
        /// <returns></returns>
        double GetPriceOutBox()
        {
            if (this.weigth <= 1000)
                return 190;
            else if (this.weigth > 1000 && this.weigth < 10000)
                return 275;
            else if (this.weigth > 10000 && this.weigth < 15000)
                return 445;
            else if (this.weigth > 15000 && this.weigth < 20000)
                return 530;
            else
                return 685;
        }

        /// <summary>
        /// Run the program
        /// </summary>
        public void Run()
        {
            double price = 0;

            this.GetPackageDimensions();

            this.type = this.GetPackageType();

            this.country = this.GetPackageDestination();

            if (this.country == Country.Inside)
            {
                if (this.type == PackageType.Letter)
                    price = this.GetPriceDenmarkLetter();
                else if (this.type == PackageType.Box)
                    price = this.GetPriceDenmarkBox();
            }
            else if (this.country == Country.Outside)
            {
                if (this.type == PackageType.Letter)
                    price = this.GetPriceOutLetter();
                else if (this.type == PackageType.Box)
                    price = this.GetPriceOutBox();
            }

            Console.WriteLine("\nThe package Information: ");
            Console.WriteLine("Dimensions: " + this.weigth + "kg " + this.length + " cm");
            Console.WriteLine("Sending to: " + this.country + " Denmark");
            Console.WriteLine("It will be send as a: " + this.type);
            Console.WriteLine("It will cost: " + price.ToString() + "Kr.");

        }
    }
}
