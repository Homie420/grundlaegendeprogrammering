﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class InputMethods : IProg
    {
        public string ProgName { get { return "Input Methods"; } }

        /// <summary>
        /// Output math results
        /// </summary>
        void MathIO()
        {
            // Get user input
            Console.WriteLine("Type in 2 numbers: ex 2,2");
            string _userInput = Console.ReadLine();

            // Convert the user input
            int _i1 = int.Parse(_userInput.Split(',')[0]);
            int _i2 = int.Parse(_userInput.Split(',')[1]);

            // Output
            Console.WriteLine("Add: {0}", _i1 + _i2);
            Console.WriteLine("Div: {0}", _i1 / _i2);
            Console.WriteLine("Mod: {0}", (_i1 % _i2));
            Console.WriteLine("Power: {0}", Math.Pow(_i1, _i2));
        }

        /// <summary>
        /// Print pythagoras
        /// </summary>
        void PythagorasMethod()
        {
            // Get user input
            Console.WriteLine("Type in 2 numbers: ex 2,2");
            string _userInput = Console.ReadLine();

            // Convert the user input
            int _i1 = Int32.Parse(_userInput.Split(',')[0]);
            int _i2 = Int32.Parse(_userInput.Split(',')[1]);
            int biggestNumber = (_i1 > _i2) ? _i1 : _i2; // Get the biggest number

            // Output
            Console.WriteLine("Pythagoras: {0}", Math.Sqrt((_i1^2)+(_i2^2)));
            Console.WriteLine("Biggest Number: {0}", biggestNumber);
            
        }

        /// <summary>
        /// The run method will be used by program.cs
        /// </summary>
        public void Run()
        {
            this.MathIO();

            this.PythagorasMethod();


        }
    }

    class LoopMethods : IProg
    {
        public string ProgName { get { return "LoopMethods"; } }

        /// <summary>
        /// Get user input and output function

        /// </summary>
        void UserInput()
        {
            Console.WriteLine("Type in the X value");
            int _userInput = Int32.Parse(Console.ReadLine());

            for (int i = 0; i < _userInput; i++)
            {
                Console.WriteLine("{0} + 32 - 297 = {1}", i, (i + 32 - 297));
            }
        }

        /// <summary>
        /// The run method
        /// </summary>
        public void Run()
        {
            this.UserInput();
        }
    }

    class SelfDefinedMethods : IProg
    {
        public string ProgName { get { return "Self Defined Methods"; } }

        List<int> l = new List<int>() { 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        void PrintList(List<int> l)
        {
            for (int i = 0; i < l.Count; i++)
            {
                Console.Write(l[i] + "\t");
            }
            Console.WriteLine("");
        }

        /// <summary>
        /// Returns the sum of 2 numbers
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        int Addition(int a, int b)
        {
            return a + b;
        }

        /// <summary>
        /// Returns the division of 2 numbers
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        int Division(int a, int b)
        {
            return a / b;
        }

        /// <summary>
        /// Returns modulus of 2 numbers
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        float Modulus(int a, int b)
        {
            return a % b;
        }

        /// <summary>
        /// Finds the biggest number in a list
        /// </summary>
        /// <param name="_list"></param>
        /// <returns></returns>
        int FindBiggestNumber(List<int> _list)
        {
            int _cN = _list[0];

            for (int i = 0; i < _list.Count; i++)
            {
                if (_list[i] > _cN)
                    _cN = _list[i];
            }

            return _cN;
        }

        /// <summary>
        /// Returns whether x is in the list
        /// </summary>
        /// <param name="list"></param>
        /// <param name="x"></param>
        /// <returns></returns>
        bool Indeholder(List<int> list, int x)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] == x)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Returns the average of a lists sum
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        float Average(List<int> list)
        {
            int _sum = 0;

            for (int i = 0; i < list.Count; i++)
            {
                _sum += list[i];
            }

            return _sum / list.Count;
        }

        /// <summary>
        /// Returns whether a list has been sorted or not
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        bool IsSorted(List<int> list)
        {
            for (int i = 0; i < list.Count-1; i++)
            {
                for (int j = i+1; j < list.Count; j++)
                {
                    if (list[i] > list[j])
                        return false;
                }
            }

            return true;
        }

        /// <summary>
        /// The run method
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Sum: 5 & 5 = {0}", this.Addition(5,5));
            Console.WriteLine("division: 5 & 5 = {0}", this.Division(5,5));
            Console.WriteLine("Modulus: 5 & 5 = {0}", this.Modulus(5,5));

            this.PrintList(l);
            Console.WriteLine("Biggest number in list {0}", this.FindBiggestNumber(l));
            Console.WriteLine("List has 5 as a element {0}", this.Indeholder(l, 5));
            Console.WriteLine("Get average of the list {0}", this.Average(l));
            Console.WriteLine("Is list sorted {0}", this.IsSorted(l));
            
        }
    }
}
