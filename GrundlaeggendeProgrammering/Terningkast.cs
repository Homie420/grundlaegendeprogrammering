﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class Terningkast : IProg
    {
        // Random library
        Random rnd = new Random();

        public string ProgName { get { return "Terningkast"; } }

        /// <summary>
        /// Gets the number which the cube represents when it has been thrown.
        /// </summary>
        /// <returns></returns>
        int GetThrow()
        {
            return rnd.Next(1, 7);
        }

        /// <summary>
        /// Runs the program
        /// simulates a throw of a cube.
        /// </summary>
        public void Run()
        {
            bool simulate = true;

            Console.WriteLine("Cube Simulator, Press N to escape from the simulation");

            // While simulate is true run infinity
            while (simulate)
            {

                int cubeNumber = this.GetThrow();

                // Check what number we got, and print out the correct message for the user.
                if (cubeNumber == 1)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("1!");
                }
                else if (cubeNumber == 2)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("2!");
                }
                else if (cubeNumber == 3)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("3!");
                }
                else if (cubeNumber == 4)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("4!");
                }
                else if (cubeNumber == 5)
                {
                    Console.ForegroundColor = ConsoleColor.DarkBlue;
                    Console.WriteLine("5!");
                }
                else if (cubeNumber == 6)
                {
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("6!");
                }

                Console.ForegroundColor = ConsoleColor.White; // Change to normal again after each throw

                // Check if user pressed the 'n' key.
                if (Console.ReadKey().Key == ConsoleKey.N)
                    simulate = false;

            }
        }
    }
}
