﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class Celciusomregner : IProg
    {
        public string ProgName { get { return "Celcius Omregner"; } }

        /// <summary>
        /// Calculates from celcius to reamur
        /// </summary>
        /// <param name="celcius"></param>
        /// <returns>The value in reamur</returns>
        double GetReamur(double celcius)
        {
            return celcius * 0.8;
        }

        /// <summary>
        /// Calculates from celcius to fahrenheit
        /// </summary>
        /// <param name="celcius"></param>
        /// <returns>The value in fahrenheit</returns>
        double GetFahrenheit(double celcius)
        {
            return celcius * 1.8 + 32;
        }

        /// <summary>
        /// Runs the program and takes in a user input which should be a string.
        /// </summary>
        /// <param name="userInput"></param>
        public void Run()
        {

            Console.WriteLine("Hvor mange grader skal der omskrives: ");
            string userInput = Console.ReadLine();

            double celcius = Double.Parse(userInput);

            double reamurVal = GetReamur(celcius);
            double fahrenheitVal = GetFahrenheit(celcius);

            Console.WriteLine("Celcius: " + userInput + " Fahrenheit: " + fahrenheitVal + " Reamur: " + reamurVal);
        }
    }
}
