﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class GaetEtTal : IProg
    {
        Random rnd = new Random();

        int secretNumber;
        int guessesUsed;

        public string ProgName { get { return "Gæt Et Tal"; } }

        /// <summary>
        /// Sets the secret integer
        /// </summary>
        void SetSecret()
        {
            this.secretNumber = this.rnd.Next(1, 1000);
            this.guessesUsed = 0;
        }

        public void Run()
        {
            Console.WriteLine("Guess the secret number");

            this.SetSecret(); // Set the secret number

            bool simulate = true;

            while (simulate)
            {

                Console.WriteLine("Your guess?");
                int guess = Int32.Parse(Console.ReadLine()); // Get the guess from the user

                // Check if the user has guessed the secret number
                if (guess == secretNumber)
                {
                    // If the user has guessed it print message after how well the user performed
                    if (this.guessesUsed > 0 && this.guessesUsed <= 10)
                        Console.WriteLine("Very well done!");
                    else if(this.guessesUsed > 10 && this.guessesUsed <= 50)
                        Console.WriteLine("You could have done better");
                    else if (this.guessesUsed > 50)
                        Console.WriteLine("That was very bad, you can do better than that!");

                    simulate = false; // Set simulation to false to out of simulation
                }
                else if (guess < secretNumber)
                    Console.WriteLine("The secret number is higher than what you guessed!");
                else if (guess > secretNumber)
                    Console.WriteLine("The secret number is lower than what you guessed");

                this.guessesUsed++; // increase guesses used.
            }
        }

    }
}
