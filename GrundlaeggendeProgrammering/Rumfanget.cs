﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class Rumfanget : IProg
    {
        public string ProgName { get { return "Rumfangs Beregner"; } }

        /// <summary>
        /// Get the volume of a box object
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        double GetVolume(double a, double b, double c)
        {
            return a * b * c;
        }

        /// <summary>
        /// Gets the userInput and converts it to a double.
        /// </summary>
        /// <returns></returns>
        double GetUserInput()
        {
            return Double.Parse(Console.ReadLine());
        }

        /// <summary>
        /// Runs the program, takes in user input and outputs the volume of the box.
        /// </summary>
        public void Run()
        {
            Console.WriteLine("The height of the box?");
            double height = this.GetUserInput();

            Console.WriteLine("The Width of the box?");
            double width = this.GetUserInput();

            Console.WriteLine("The length of the box?");
            double length = this.GetUserInput();

            Console.WriteLine("The volume of the box is: " + this.GetVolume(height, width, length));
        }
    }
}
