﻿using System;
using System.IO;

namespace GrundlaeggendeProgrammering
{
    class FileMethods : IProg
    {
        public string ProgName { get { return "File Methods"; } }

        const string filePath = @".\StarWars.txt";

        string[] actors = new string[] { "Mark Hamil", "Harrison Ford", "Carrie Fisher" };

        /// <summary>
        /// Write Han skød først to the file path given as parameter
        /// </summary>
        /// <param name="path"></param>
        void WriteToFile(string path)
        {

            File.WriteAllText(path, "Han Skød Først");

            Console.WriteLine("Han Skød Først has been written check {0}", path);
        }

        /// <summary>
        /// Read the content of the file given as parameter and print to console
        /// </summary>
        /// <param name="path"></param>
        void ReadContent(string path)
        {
            string content = File.ReadAllText(path);

            Console.WriteLine(content);
        }

        /// <summary>
        /// Reads movies.txt and prints the content to the console
        /// </summary>
        void FileStreamer()
        {
            string _fileToRead = @".\movies.txt";

            FileStream fs = new FileStream(_fileToRead, FileMode.Open);
            StreamReader fr = new StreamReader(fs);

            while (!fr.EndOfStream)
            {
                string line = fr.ReadLine();
                Console.WriteLine(line);
            }
            fr.Close();
            fs.Close();
        }

        /// <summary>
        /// Writes the actor array to a file called actors.txt
        /// </summary>
        void FileWriting()
        {
            string _fileToRead = @".\actors.txt";

            FileStream fs = new FileStream(_fileToRead, FileMode.OpenOrCreate);
            StreamWriter sw = new StreamWriter(fs);

            foreach(string x in actors)
                sw.WriteLine(x);

            sw.Close();
            fs.Close();
        }

        // OPGAVE 7

        /// <summary>
        /// Adds a file to root path
        /// </summary>
        void AddFileToRoot(string _fileName)
        {
            FileStream fs = File.Create(@".\" + _fileName);
            fs.Close();
        }

        /// <summary>
        /// Deletes a file
        /// </summary>
        /// <param name="_filename"></param>
        bool DeleteFile(string _filename)
        {
            string _filePath = @".\" + _filename;
            if (File.Exists(_filePath))
            {
                File.Delete(@".\" + _filename);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get all the files in root
        /// </summary>
        /// <returns></returns>
        string[] GetFilesRoot()
        {
            return Directory.GetFiles(@".\", "*");
        }

        /// <summary>
        /// Returns true or false if the directory could be created
        /// </summary>
        /// <param name="_dirName"></param>
        /// <returns></returns>
        bool CreateDir(string _dirName)
        {
            string _filePath = @".\" + _dirName;

            if (!Directory.Exists(_filePath))
            {
                Directory.CreateDirectory(_filePath);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Search for a file in the root
        /// </summary>
        /// <param name="element"></param>
        /// <returns></returns>
        string SearchFile(string element)
        {
            string[] _files = Directory.GetFiles(@".\");

            for (int i = 0; i < _files.Length; i++)
            {
                if (_files[i].Contains(element))
                    return _files[i];
            }

            return null;
        }

        /// <summary>
        /// Get all the jpg files
        /// </summary>
        /// <returns></returns>
        string[] GetJpegFiles()
        {
            return Directory.GetFiles(@".\", "*.jpg");
        }

        /// <summary>
        /// The start menu and the gui method
        /// </summary>
        void StartMenuGUI()
        {
            Console.WriteLine("=====================================");
            Console.WriteLine("      H1 Queue Operations Menu       ");
            Console.WriteLine("=====================================");

            Console.WriteLine("1. Add File\n");
            Console.WriteLine("2. Delete File\n");
            Console.WriteLine("3. Display files\n");
            Console.WriteLine("4. Add Folder\n");
            Console.WriteLine("5. Search File\n");
            Console.WriteLine("6. Show Jpegs\n");
            Console.WriteLine("?. Exit\n");

            Console.WriteLine("Enter Choice: ");

            int _userInput = Int32.Parse(Console.ReadLine());
            string _userInputStr;

            switch(_userInput)
            {
                case 1:
                    Console.WriteLine("|Add File|");
                    Console.Write("Enter file name: ");
                    _userInputStr = Console.ReadLine();

                    this.AddFileToRoot(_userInputStr);

                    Console.WriteLine("{0} Was Created", _userInputStr);
                    this.StartMenuGUI();
                    break;
                case 2:
                    Console.WriteLine("|Delete File|");
                    Console.Write("Enter File Name: ");

                    _userInputStr = Console.ReadLine();

                    if (this.DeleteFile(_userInputStr))
                        Console.WriteLine(_userInputStr + " Was Deleted");
                    else
                        Console.WriteLine(_userInputStr + " Was Not Deleted");
                    this.StartMenuGUI();
                    break;
                case 3:
                    Console.WriteLine("|Display Files|");
                    string[] _files = this.GetFilesRoot();
                    for (int i = 0; i < _files.Length; i++)
                    {
                        Console.WriteLine(_files[i]);
                    }
                    this.StartMenuGUI();
                    break;
                case 4:
                    Console.WriteLine("|Delete File|");
                    Console.Write("Enter File Name: ");

                    _userInputStr = Console.ReadLine();
                    if (this.CreateDir(_userInputStr))
                        Console.WriteLine(_userInputStr + " was created");
                    else
                        Console.WriteLine(" Directory was not created");
                    this.StartMenuGUI();
                    break;
                case 5:
                    Console.WriteLine("|Search File|");
                    Console.Write("Enter name for file: ");

                    _userInputStr = Console.ReadLine();
                    string _element = this.SearchFile(_userInputStr);

                    if (_element != null)
                        Console.WriteLine("{0} Was found", _element);
                    else
                        Console.WriteLine("Nothing was found");

                    this.StartMenuGUI();
                    break;
                case 6:
                    Console.WriteLine("|Finding Jpeg's|");
                    string[] _jpgFiles = this.GetJpegFiles();
                    for (int i = 0; i < _jpgFiles.Length; i++)
                    {
                        Console.WriteLine(_jpgFiles[i]);
                    }

                    this.StartMenuGUI();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// The run method, is used in program.cs
        /// </summary>
        public void Run()
        {
            this.WriteToFile(filePath);

            this.ReadContent(filePath);

            this.FileStreamer();

            this.FileWriting();

            this.StartMenuGUI();
        }
    }
}
