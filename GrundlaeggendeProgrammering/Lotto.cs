﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class Lotto : IProg
    {
        public string ProgName { get { return "Lotto"; } }

        Random rnd = new Random();

        int[] winNumbers;
        int[] userCoupon;
        const int nmbTotal = 7; // The number of numbers in a coupon

        /// <summary>
        /// Generate the winning coupon
        /// </summary>
        /// <returns></returns>
        void GenerateWinningNumbers()
        {
            int[] tmpArr = new int[nmbTotal];

            for (int i = 0; i < tmpArr.Length; i++)
            {
                tmpArr[i] = rnd.Next(1, 20);
            }

            winNumbers = tmpArr;
        }

        /// <summary>
        /// Get user input and convert to array
        /// </summary>
        void GetUserCoupon()
        {
            Console.WriteLine("Type in your coupon numbers ex: 1,2,3,4");
            int[] tmpArr = Array.ConvertAll(Console.ReadLine().Split(','), int.Parse); // Get all the user data to int array

            if (tmpArr.Length != nmbTotal)
                this.GetUserCoupon();

            userCoupon = tmpArr;
        }

        /// <summary>
        /// Check the user coupon and the winning coupon
        /// returns how many is correct
        /// </summary>
        int CheckCoupons()
        {
            int counter = 0;

            for (int i = 0; i < nmbTotal; i++)
            {
                if (winNumbers[i] == userCoupon[i])
                    counter++;
            }

            return counter;
        }


        public void Run()
        {
            this.GenerateWinningNumbers();
            this.GetUserCoupon();
            int correct = this.CheckCoupons();

            switch(correct)
            {
                case 2:
                    Console.WriteLine("Congratulation you have won 50kr");
                    break;
                case 3:
                    Console.WriteLine("Congratulation you have won 250kr");
                    break;
                case 4:
                    Console.WriteLine("Congratulation you have won 1000kr");
                    break;
                case 5:
                    Console.WriteLine("Congratulation you have won 10.000kr");
                    break;
                case 6:
                    Console.WriteLine("Congratulation you have won 50.000kr");
                    break;
                case 7:
                    Console.WriteLine("Congratulation you have won 1.000.000kr");
                    break;
                default:
                    Console.WriteLine("You Won Nothing!");
                    break;
            }
            Console.Write("The Winning Numbers: ");
            Array.ForEach(winNumbers, x => Console.Write(x + " ")); // Lambda function to write each winning number
            Console.WriteLine("\nYou had {0} correct", correct);


        }
    }
}
