﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class Alder : IProg
    {
        public string ProgName { get { return "Alder"; } }

        /// <summary>
        /// Run the program and get the user input for a name and an age, print out the correct message for the user.
        /// </summary>
        public void Run()
        {
            // Get the users name.
            Console.WriteLine("Type in your name: ");
            string userName = Console.ReadLine();

            // Get the user age
            Console.WriteLine("Type in your age: ");
            int userAge = Int32.Parse(Console.ReadLine());

            // Check how old the user is.
            if (userAge < 3)
                Console.WriteLine(userName + " You should still be in diapers");
            else if (userAge > 3 && userAge <= 15)
                Console.WriteLine(userName + " You can't do nothing!");
            else if (userAge >= 16 && userAge < 18)
                Console.WriteLine(userName + " You can not drink yet");
            else
                Console.WriteLine(userName + " You can drink and drive. But not at the same time!");
        }
    }
}
