﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    #region User login System
    class UserApplication : IProg
    {

        #region User Class
        /// <summary>
        /// Class for an user
        /// </summary>
        class User
        {
            public string Usernanme { get { return this.username; } }
            public string Password { get { return this.password; } }

            string username;
            string password;

            public User(string username, string password)
            {
                this.username = username;
                this.password = password;
            }
        };

        // The array that holds all the users
        User[] userDB = new User[5];
        
        #endregion

        int tryCounter = 0; // how many tries the user has tried to login

        public string ProgName { get { return "Login System"; } }

        /// <summary>
        /// Create new user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        bool CreateUser(string username, string password)
        {
            for (int i = 0; i < userDB.Length; i++)
            {
                if (userDB[i] == null)
                {
                    userDB[i] = new User(username, password);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Logins the user
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        bool LoginUser(string username, string password)
        {
            User curUser = null; // The currentUser in the progress

            // Look through array to find a user that match the username
            for (int i = 0; i < userDB.Length; i++)
            {
                curUser = userDB[i];

                if (curUser != null)
                    if (curUser.Usernanme == username)
                        i = userDB.Length + 1;
            }


            // Check if we found an user and check if password matches
            if (curUser != null)
            {
                if (curUser.Password == password)
                    return true;
                else
                {
                    Console.WriteLine("Password did not match to user");
                    return false;
                }
            }else
            {
                Console.WriteLine("No username was found");
                return false;
            }
        }

        /// <summary>
        /// The menu where the user can create an account
        /// </summary>
        void CreateUserMenu()
        {
            Console.WriteLine("Create An User");

            Console.WriteLine("Write your username");
            string userName = Console.ReadLine();

            Console.WriteLine("Write your password");
            string password = Console.ReadLine();


            if (this.CreateUser(userName, password))
            {
                Console.WriteLine("Account was created");
                this.StartMenu();
            }
            else
                Console.WriteLine("The number of accounts has reached it's limits");
        }

        /// <summary>
        /// The login user menu
        /// </summary>
        void LoginUserMenu()
        {

            Console.WriteLine("Login");
            Console.WriteLine("Type in username");
            string username = Console.ReadLine();

            Console.WriteLine("Type in Password");
            string password = Console.ReadLine();

            if (this.tryCounter < 3)
            {
                if (this.LoginUser(username, password))
                    this.Userlogined(username);
                else
                {
                    this.tryCounter++;
                    this.LoginUserMenu();
                }

            }
        }

        /// <summary>
        /// Show if a user has logined
        /// </summary>
        /// <param name="username"></param>
        void Userlogined(string username)
        {
            Console.WriteLine("Congratulation you have been logined as " + username);
        }

        /// <summary>
        /// The startmenu
        /// </summary>
        void StartMenu()
        {
            Console.WriteLine("Type 1 to create a user. Type 2 to login");
            string answer = Console.ReadLine();

            if (answer == "1")
                this.CreateUserMenu();
            else if (answer == "2")
                this.LoginUserMenu();
        }

        /// <summary>
        /// Run the program
        /// </summary>
        public void Run()
        {
            Console.WriteLine("You have to login to get past this program");

            this.tryCounter = 0;

            this.StartMenu();
        }

    }

    #endregion

    class BoyNameSearcher : IProg
    {
        // name array
        string[] namesArr = new string[]
        {
            "William",
            "Oliver",
            "Noah",
            "Emil",
            "Victor",
            "Magnus",
            "Frederik",
            "Mikkel",
            "Lucas",
            "Alexander",
            "Oscar",
            "Mathias",
            "Sebastian",
            "Malthe",
            "Elias",
            "Christian",
            "Mads",
            "Gustav",
            "Villads",
            "Tobias"
        };

        public string ProgName { get { return "Boy Names"; } }

        /// <summary>
        /// Seach a name
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        string SearchName(string word)
        {
            word = word.ToLower();

            for (int i = 0; i < namesArr.Length; i++)
            {
                string curName = namesArr[i].ToLower();

                if (curName.Contains(word))
                    return namesArr[i];
            }

            return null;
        }

        /// <summary>
        /// Order names after alphabetic order
        /// </summary>
        /// <returns></returns>
        string[] OrderNames()
        {
            string[] tmpArr = namesArr;

            Array.Sort(tmpArr);

            return tmpArr;
        }

        /// <summary>
        /// The searchMenu
        /// </summary>
        void SearchMenu()
        {
            Console.WriteLine("Type in what you want to search for");
            string userInput = Console.ReadLine();

            Console.WriteLine(this.SearchName(userInput) + " Was found");

            this.StartMenu();
        }

        /// <summary>
        /// The ordered menu
        /// </summary>
        void OrderNamesMenu()
        {
            string[] orderedNames = this.OrderNames();

            for (int i = 0; i < orderedNames.Length; i++)
            {
                Console.WriteLine(orderedNames[i]);
            }

            this.StartMenu();
        }

        /// <summary>
        /// The startmenu
        /// </summary>
        void StartMenu()
        {
            Console.WriteLine("Type 1 to search for a name. Type 2 to get all names ordered. Type 3 to exit");
            string userInput = Console.ReadLine();

            if (userInput == "1")
                this.SearchMenu();
            else if (userInput == "2")
                this.OrderNamesMenu();
            else
                Console.WriteLine("Ended");
        }

        /// <summary>
        /// This runs the program
        /// </summary>
        public void Run()
        {
            this.StartMenu();
        }
    }

    class BoysAndGirlsName : IProg
    {
        // name array
        List<string> boyNamesArr = new List<string>()
        {
            "William",
            "Oliver",
            "Noah",
            "Emil",
            "Victor",
            "Magnus",
            "Frederik",
            "Mikkel",
            "Lucas",
            "Alexander",
            "Oscar",
            "Mathias",
            "Sebastian",
            "Malthe",
            "Elias",
            "Christian",
            "Mads",
            "Gustav",
            "Villads",
            "Tobias"
        };

        List<string> girlNamesArr = new List<string>()
        {
            "Emma",
            "Ida",
            "Clara",
            "Laura",
            "Isabella",
            "Sofia",
            "Sofie",
            "Anna",
            "Mathilde",
            "Freja",
            "Caroline",
            "Lærke",
            "Maja",
            "Josefine",
            "Liva",
            "Alberte",
            "Karla",
            "Victoria",
            "Olivia",
            "Alma"
        };

        public string ProgName { get { return "Boys And Girls Program"; } }

        /// <summary>
        /// Find names in the boy array and the girl array
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        string[] SearchNames(string word)
        {
            // Join both arrays
            string[] allNamesArr = new string[boyNamesArr.Count + girlNamesArr.Count];
            Array.Copy(boyNamesArr.ToArray(), allNamesArr, boyNamesArr.Count);
            Array.Copy(girlNamesArr.ToArray(), 0, allNamesArr, boyNamesArr.Count, girlNamesArr.Count);

            // The names that is found
            List<string> foundNames = new List<string>();

            word = word.ToLower();

            // Search array for the name and add to found array
            for (int i = 0; i < allNamesArr.Length; i++)
            {
                string _name = allNamesArr[i].ToLower();

                if (_name.Contains(word))
                    foundNames.Add(allNamesArr[i]);
            }

            return foundNames.ToArray();
        }

        /// <summary>
        /// Order all the names both for boys and girls
        /// </summary>
        /// <returns></returns>
        string[] OrderNames()
        {
            // Join both arrays
            string[] allNamesArr = new string[boyNamesArr.Count + girlNamesArr.Count];
            Array.Copy(boyNamesArr.ToArray(), allNamesArr, boyNamesArr.Count);
            Array.Copy(girlNamesArr.ToArray(), 0, allNamesArr, boyNamesArr.Count, girlNamesArr.Count);

            Array.Sort(allNamesArr);

            return allNamesArr;
        }

        /// <summary>
        /// Add a boy name to the boy array
        /// </summary>
        /// <param name="name"></param>
        void AddBoyName(string name)
        {
            this.boyNamesArr.Add(name);
        }

        /// <summary>
        /// Add a girl name to the girl array
        /// </summary>
        /// <param name="name"></param>
        void AddGirlName(string name)
        {
            this.girlNamesArr.Add(name);
        }

        /// <summary>
        /// Remove a name from the a list
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool RemoveName(string name)
        {
            name = name.ToLower();

            // Search the boy array
            for (int i = 0; i < boyNamesArr.Count; i++)
            {
                string _name = boyNamesArr[i].ToLower();

                if (_name == name)
                {
                    boyNamesArr.RemoveAt(i);
                    return true;
                }
            }

            // Search girl array
            for (int i = 0; i < girlNamesArr.Count; i++)
            {
                string _name = girlNamesArr[i].ToLower();

                if (_name == name)
                {
                    girlNamesArr.RemoveAt(i);
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// The add boy name menu
        /// </summary>
        void AddBoyNameMenu()
        {
            Console.WriteLine("Type in the name you want to add");
            string userInput = Console.ReadLine();

            this.AddBoyName(userInput);
            Console.WriteLine(userInput + " Was added");

            this.StartMenu();
        }

        /// <summary>
        /// The add boy name menu
        /// </summary>
        void AddGirlNameMenu()
        {
            Console.WriteLine("Type in the name you want to add");
            string userInput = Console.ReadLine();

            this.AddGirlName(userInput);
            Console.WriteLine(userInput + " Was added");

            this.StartMenu();
        }

        /// <summary>
        /// Remove a name menu
        /// </summary>
        void RemoveNameMenu()
        {
            Console.WriteLine("Type in the name you want to remove");
            string userInput = Console.ReadLine();

            if (this.RemoveName(userInput))
                Console.WriteLine(userInput + " Has been removed");
            else
                Console.WriteLine(userInput + "Doesn't exist");

            this.StartMenu();
        }



        /// <summary>
        /// The searchMenu
        /// </summary>
        void SearchMenu()
        {
            Console.WriteLine("Type in what you want to search for");
            string userInput = Console.ReadLine();

            string[] _names = this.SearchNames(userInput);

            for (int i = 0; i < _names.Length; i++)
            {
                Console.WriteLine(_names[i] + " Was found");
            }

            this.StartMenu();
        }

        /// <summary>
        /// The ordered menu
        /// </summary>
        void OrderNamesMenu()
        {
            // Boys

            string[] orderedNames = this.OrderNames();

            for (int i = 0; i < orderedNames.Length; i++)
            {
                Console.WriteLine(orderedNames[i]);
            }

            this.StartMenu();
        }

        /// <summary>
        /// The startmenu
        /// </summary>
        void StartMenu()
        {
            Console.WriteLine("Type 1 to search for a name.");
            Console.WriteLine("Type 2 to get all names ordered.");
            Console.WriteLine("Type 3 to add a name to the boys.");
            Console.WriteLine("Type 4 to Add a name to the girls.");
            Console.WriteLine("Type 5 to remove a name.");
            Console.WriteLine("Type anything else to exit");

            string userInput = Console.ReadLine();

            switch(userInput)
            {
                case "1":
                    this.SearchMenu();
                    break;
                case "2":
                    this.OrderNamesMenu();
                    break;
                case "3":
                    this.AddBoyNameMenu();
                    break;
                case "4":
                    this.AddGirlNameMenu();
                    break;
                case "5":
                    this.RemoveNameMenu();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// This runs the program
        /// </summary>
        public void Run()
        {
            this.StartMenu();
        }
    }

    class Karakterer : IProg
    {
        public string ProgName { get { return "Karakterer"; } }

        int[,] grades = new int[2, 10];

        /// <summary>
        /// Give all the students grades
        /// </summary>
        void GiveGrades()
        {
            for (int i = 0; i < grades.GetLength(0); i++)
            {
                for (int j = 0; j < grades.GetLength(1); j++)
                {
                    Console.WriteLine("Give group {0} student {1} a grade", i+1, j+1);
                    grades[i, j] = Int32.Parse(Console.ReadLine());
                }
            }
        }

        /// <summary>
        /// Get the average of all the students in the 2 groups
        /// </summary>
        /// <returns></returns>
        double GetAverageGrades()
        {
            double _finVal = 0;

            for (int i = 0; i < grades.GetLength(0); i++)
            {
                for (int j = 0; j < grades.GetLength(1); j++)
                {
                    _finVal += grades[i, j];
                }
            }

            return _finVal / (grades.Length);
        }

        // Run the program
        public void Run()
        {
            this.GiveGrades();
            Console.WriteLine("The average of the 2 groups is {0}", this.GetAverageGrades());
        }
    }

    class StatistikProgram : IProg
    {
        public string ProgName { get { return "Statistisk Program"; } }

        float[] data = new float[1000]; // The array which holds the data

        /// <summary>
        /// Get the count of elements which exists in the data array
        /// </summary>
        /// <returns></returns>
        int GetElementCount()
        {
            int _length = 0;

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] != 0)
                    _length++;
            }

            return _length;
        }

        /// <summary>
        /// Adds data to the array
        /// </summary>
        /// <param name="number"></param>
        bool AddData(float number)
        {
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == 0)
                {
                    data[i] = number;
                    return true;
                }

            }

            return false;
        }

        /// <summary>
        /// Gets the sum of all the values
        /// </summary>
        /// <returns></returns>
        float GetSum()
        {
            float sum = 0;

            for (int i = 0; i < data.Length; i++)
            {
                sum += data[i];
            }

            return sum;
        }

        /// <summary>
        /// Gets the minimum number in the data array
        /// </summary>
        /// <returns></returns>
        float GetMinimum()
        {
            float _curMin = data[0];

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] != 0)
                    if (_curMin > data[i])
                        _curMin = data[i];
            }

            return _curMin;
        }

        /// <summary>
        /// Gets the maximum number in the data array
        /// </summary>
        /// <returns></returns>
        float GetMaximum()
        {
            float _curMax = data[0];

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] != 0)
                    if (_curMax < data[i])
                        _curMax = data[i];
            }

            return _curMax;
        }

        /// <summary>
        /// Find all the elements of a number
        /// </summary>
        void FindElementMenu()
        {
            Console.WriteLine("Type in a number you would like to search for");
            float userInput = float.Parse(Console.ReadLine());

            int _elements = 0;

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == userInput)
                {
                    Console.WriteLine("The element {0} is at index {1}", userInput, i+1);
                    _elements++;
                }
            }

            Console.WriteLine("There was a total of {0}", _elements);

            this.StartMenu();
        }

        /// <summary>
        /// The menu where data can be added
        /// </summary>
        void AddDataMenu()
        {
            Console.WriteLine("Type in the data you want to add");
            float userInput = float.Parse(Console.ReadLine());

            if (this.AddData(userInput))
                Console.WriteLine(userInput + " Was added");
            else
                Console.WriteLine("The maximum data has been reached");

            this.StartMenu();
        }

        /// <summary>
        /// Shows all the elements
        /// </summary>
        void ShowAllElementsMenu()
        {
            Console.WriteLine("Showing all data");

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] != 0)
                    Console.WriteLine("{0}. {1}", i+1, data[i]);
            }

            this.StartMenu();
        }

        /// <summary>
        /// The statistics menu
        /// </summary>
        void GetAllStatisticsMenu()
        {
            Console.WriteLine("Statistics:");
            int _elements = this.GetElementCount();
            float _sum = this.GetSum();
            float _avg = _sum / _elements;
            float _min = this.GetMinimum();
            float _max = this.GetMaximum();

            Console.WriteLine("There is {0} elements. The sum is {1}. The average number is {2}. The minimum is {3}. The Maximum is {4}", _elements, _sum, _avg, _min, _max);


            this.StartMenu();
        }
        
        /// <summary>
        /// The startmenu
        /// </summary>
        void StartMenu()
        {
            Console.WriteLine("Statistik Program");

            Console.WriteLine("Type 1. to add new data");
            Console.WriteLine("Type 2. see all data");
            Console.WriteLine("Type 3. find an element");
            Console.WriteLine("Type 4. see statistics");
            Console.WriteLine("Type any other key to exit");

            string userInput = Console.ReadLine();

            switch(userInput)
            {
                case "1":
                    this.AddDataMenu();
                    break;
                case "2":
                    this.ShowAllElementsMenu();
                    break;
                case "3":
                    this.FindElementMenu();
                    break;
                case "4":
                    this.GetAllStatisticsMenu();
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// This method runs the program
        /// </summary>
        public void Run()
        {
            this.StartMenu();
        }
    }

    class ExcelTable : IProg
    {
        public string ProgName { get { return "Excel Tabel"; } } // The name of the program which will be shown when the program runs

        Random rnd = new Random();

        int[,] tArray = new int[5, 5];

        /// <summary>
        /// Adds random values to the table
        /// </summary>
        void AddRandomvalues()
        {
            for (int i = 0; i < tArray.GetLength(0); i++)
            {
                for (int j = 0; j < tArray.GetLength(1); j++)
                {
                    tArray[i, j] = rnd.Next(1, 25);
                }
            }
        }

        /// <summary>
        /// Show the table
        /// </summary>
        void ShowTable()
        {
            for (int i = 0; i < tArray.GetLength(0); i++)
            {
                for (int j = 0; j < tArray.GetLength(1); j++)
                {
                    Console.Write(tArray[i, j] + "\t");
                }
                Console.WriteLine("\n" + "");
            }
        }


        /// <summary>
        /// The run method. Will Run the program
        /// </summary>
        public void Run()
        {
            this.AddRandomvalues();

            this.ShowTable();
        }
    }

    class EditableExcelTable : IProg
    {
        public string ProgName { get { return "Editable Excel Table"; } } // The name of the mini program

        Random rnd = new Random();

        int[,] tArray = new int[5, 5];

        /// <summary>
        /// Adds random values to the table
        /// </summary>
        void AddRandomvalues()
        {
            for (int i = 0; i < tArray.GetLength(0); i++)
            {
                for (int j = 0; j < tArray.GetLength(1); j++)
                {
                    tArray[i, j] = rnd.Next(1, 25);
                }
            }
        }

        /// <summary>
        /// Show the table
        /// </summary>
        void ShowTable()
        {
            for (int i = 0; i < tArray.GetLength(0); i++)
            {
                for (int j = 0; j < tArray.GetLength(1); j++)
                {
                    Console.Write(tArray[i, j] + "\t");
                }
                Console.WriteLine("\n" + "");
            }
        }

        /// <summary>
        /// Takes the coordinates of two 'CELLS' and returns the sum
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="y1"></param>
        /// <param name="x2"></param>
        /// <param name="y2"></param>
        /// <returns></returns>
        int AddTwoValues(int x1, int y1, int x2, int y2)
        {
            return tArray[x1, y1] + tArray[x2, y2];
        }

        /// <summary>
        /// The menu where two values can be added
        /// </summary>
        void AddValuesMenu()
        {
            Console.WriteLine("Type in the first values coordinates x,y");
            string userInput = Console.ReadLine();
            int x1 = Int32.Parse(userInput.Split(',')[0]); // Get the x coordinate
            int y1 = Int32.Parse(userInput.Split(',')[1]); // Get the y coordinate

            Console.WriteLine("Type in the second values coordinates with ex: x,y");
            userInput = Console.ReadLine();
            int x2 = Int32.Parse(userInput.Split(',')[0]);  // Get the x coordinate
            int y2 = Int32.Parse(userInput.Split(',')[1]); // Get the y coordinate

            int sum = this.AddTwoValues(x1, y1, x2, y2);

            Console.WriteLine("[{0},{1}] {2} + [{3},{4}] {5} = {6}", x1, x2, tArray[x1, y1], x2, y2, tArray[x2, y2], sum);

            this.UserStarteMenu();
        }


        /// <summary>
        /// The start menu
        /// </summary>
        void UserStarteMenu()
        {
            Console.WriteLine("Type 1. to add values together");
            Console.WriteLine("Type any other key to exit");

            if (Console.ReadLine() == "1")
                this.AddValuesMenu();

        }

        /// <summary>
        /// The run method. Will Run the program
        /// </summary>
        public void Run()
        {
            this.AddRandomvalues();

            this.ShowTable();

            this.AddValuesMenu();
        }
    }
}
