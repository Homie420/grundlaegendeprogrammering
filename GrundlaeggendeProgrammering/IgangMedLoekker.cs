﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GrundlaeggendeProgrammering
{
    class IgangMedLoekker : IProg
    {
        public string ProgName { get { return "I Gang Med Løkker"; } }

        /// <summary>
        /// For loop that outputs the number from 0 - 99;
        /// </summary>
        void OpgaveA()
        {
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine(i);
            }
        }

        /// <summary>
        /// A for loop that outputs the number from 0 - 49
        /// </summary>
        void OpgaveB()
        {
            for (int i = 0; i < 100; i++)
            {
                if (i < 50)
                    Console.WriteLine(i);
            }
        }

        /// <summary>
        /// A while loop that outputs from 0 - 49
        /// </summary>
        void OpgaveC()
        {
            int i = 0;

            while (i < 50)
            {
                Console.WriteLine(i);
                i++;
            }
        }


        /// <summary>
        /// A for loop that outputs from 100 - 0
        /// </summary>
        void OpgaveD()
        {
            for (int i = 100; i >= 0 ; i--)
            {
                Console.WriteLine(i);
            }
        }

        /// <summary>
        /// Run all the examples
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Running Loops:");
            Console.WriteLine("A");
            this.OpgaveA();

            Console.WriteLine("B");
            this.OpgaveB();

            Console.WriteLine("C");
            this.OpgaveC();

            Console.WriteLine("D");
            this.OpgaveD();
        }
    }
}
